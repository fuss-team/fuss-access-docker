#!/bin/bash

grep ServerName /etc/apache2/sites-enabled/*|awk '{print $3;};' > /tmp/old-domains.txt

cp /etc/apache2/sites-enabled/50-_access_fuss_bz_it.conf /tmp/access.old
docker exec fuss-access python3 /var/www/fuss-access-landing/manage.py vhosts > /etc/apache2/sites-enabled/50-_access_fuss_bz_it.conf

grep ServerName /etc/apache2/sites-enabled/*|awk '{print $3;};' > /tmp/new-domains.txt

if ! diff /tmp/old-domains.txt /tmp/new-domains.txt ; then
	certbot certonly --webroot -w /var/www/html --expand --non-interactive -d $(xargs < /tmp/new-domains.txt | sed 's/ /,/g')
fi

if ! diff /tmp/access.old /etc/apache2/sites-enabled/50-_access_fuss_bz_it.conf ; then
	systemctl restart apache2
fi

rm /tmp/access.old
rm /tmp/old-domains.txt
rm -f /tmp/new-domains.txt
