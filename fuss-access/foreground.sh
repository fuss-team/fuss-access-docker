#!/bin/bash

cd /var/www/fuss-access-landing

if [ -e /var/lib/access/settings_local.py ] && [ ! -e access_landing/settings_local.py ] ; then
	ln -s /var/lib/access/settings_local.py access_landing/settings_local.py
fi

python3 manage.py collectstatic --noinput
python3 manage.py makemigrations access_landing
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py compilemessages

source /etc/apache2/envvars
apache2 -D FOREGROUND
